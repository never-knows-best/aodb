﻿using AODB.Common.DbClasses;
using System;
using System.IO;

namespace AODB.Common.RDBObjects
{
    public class Image : RDBObject
    {
        public byte[] JpgData;

        public override void Deserialize(BinaryReader reader)
        {
            JpgData = reader.ReadBytes((int)reader.BaseStream.Length);
        }


        public override byte[] Serialize()
        {
            throw new NotImplementedException();
        }
    }
}
