﻿using System;

namespace AODB.Common.DbClasses
{
    public class RDBDoNotSerializeAttribute : Attribute
    {
        public RDBDoNotSerializeAttribute() { }
    }
}
